<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        
        //$p = $em->getRepository('AppBundle:prueba')->find(1);
        //echo $p->getNombre().' '.$p->getApellido().' '.$p->getProfe();
        
        $p = $em->getRepository('AppBundle:prueba')->findAll();
        
        foreach($p as $pp){
            echo $pp->getNombre();
        }
        
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}
